import { combineReducers } from 'redux';

import chat from './chat';
import network from './network';
import voice from './voice';

const rootReducer = combineReducers({
    chat,
    network,
    voice
});

export default rootReducer;