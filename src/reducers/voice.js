import {RECORDING_VOICE, VOICE_INIT} from '../actions/voice';

const defaultState = {
    recording: false,
    enabled: false
};

function voiceReducer(state = defaultState, action) {

    switch(action.type) {
        case RECORDING_VOICE:
            return {
                ...state,
                recording: action.payload
            };

        case VOICE_INIT:
            return {
                ...state,
                enabled: true
            };

        default:
            return state;
    }

}

export default voiceReducer;