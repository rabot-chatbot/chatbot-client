import {NEW_MESSAGE} from '../actions/chat';

const defaultState = {
    messages: []
};

function chatReducer(state = defaultState, action) {

    switch(action.type) {
        case NEW_MESSAGE:
            return {
                ...state,
                messages: [
                    action.payload,
                    ...state.messages
                ]
            };

        default:
            return state;
    }

}

export default chatReducer;