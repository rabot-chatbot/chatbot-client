import {ONLINE} from '../actions/network';

const defaultState = {
    online: navigator.onLine
};

function networkReducer(state = defaultState, action) {

    switch(action.type) {
        case ONLINE:
            return {
                ...state,
                online: action.payload
            };

        default:
            return state;
    }

}

export default networkReducer;