export const START_SEND_MESSAGE = 'START_SEND_MESSAGE';
export const NEW_MESSAGE = 'NEW_MESSAGE';

export const START_SEND_INTENT = 'START_SEND_INTENT';
export const INTENT_SEND = 'INTENT_SEND';

export const PACKET_SEND = 'PACKET_SEND';

export const sendMessage = (utterance) => ({
    type: START_SEND_MESSAGE,
    payload: {
        utterance
    }
});

export const sendIntent = (app, intent, replyTo = '') => ({
    type: START_SEND_INTENT,
    payload: {
        app,
        intent,
        replyTo
    }
});

export const newMessage = (message, channel) => ({
    type: NEW_MESSAGE,
    payload: {
        message,
        channel
    }
});