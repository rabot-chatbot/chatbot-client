export const START_VOICE_RECORDING = 'START_VOICE_RECORDING';
export const STOP_VOICE_RECORDING = 'STOP_VOICE_RECORDING';
export const RECORDING_VOICE = 'RECORDING_VOICE';
export const VOICE_INIT = 'VOICE_INIT';

export const recordVoice = () => ({
    type: START_VOICE_RECORDING
});

export const stopRecordVoice = () => ({
    type: STOP_VOICE_RECORDING
});

export const recordingVoice = (payload) => ({
    type: RECORDING_VOICE,
    payload
});

export const initVoice = (payload) => ({
    type: VOICE_INIT
});