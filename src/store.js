import {rootEpic} from './epics';
import reducers from './reducers';

import {createStore, applyMiddleware, compose} from 'redux';
import {createEpicMiddleware} from 'redux-observable';

import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const epicMiddleware = createEpicMiddleware();

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const persistConfig = {
    key: 'root',
    storage,
    blacklist: ['voice', 'network']
};

export default () => {
    const persistedReducers = persistReducer(persistConfig, reducers);

    const store = createStore(
        persistedReducers,
        composeEnhancer(
            applyMiddleware(epicMiddleware),
        )
    );

    epicMiddleware.run(rootEpic);

    const persistor = persistStore(store);

    return {
        store,
        persistor
    };
}