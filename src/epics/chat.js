import {ofType} from 'redux-observable';
import {fromEvent, from} from 'rxjs';
import {switchMap, map, catchError, filter} from 'rxjs/operators';
import Cookies from 'js-cookie';

import {
    APP_INIT,
} from '../actions';

import {
    START_SEND_MESSAGE,
    NEW_MESSAGE,
    PACKET_SEND,
    START_SEND_INTENT, INTENT_SEND
} from '../actions/chat';

import {connect} from 'async-mqtt';

const username = 'vincent';
export const channelChatBot = `rabot/chat/${username}/chatbot`;
export const channelUserMessage = `rabot/chat/${username}/user/message`;
export const channelUserIntent = `rabot/chat/${username}/user/intent`;

/**
 * Get auth token
 */
const authToken = Cookies.get('rab-auth');

/**
 * Connect to MQTT.js and listen to channel rabot/message
 */
const client = connect(process.env.REACT_APP_RABBIT_MQ_WEBSOCKET, {
    username: authToken,
    password: '',
    clientId: `${username}-webapp`,
    clean: false
});

client.subscribe(channelChatBot, {qos: 1});

/**
 * Listen to events from MQTT.js
 * @type {Observable<T>}
 */
const messageEvent$ = fromEvent(client, 'message');
const packetsSendEvent$ = fromEvent(client, 'packetsend');

/**
 * Listen to incoming messages
 * @param action$
 * @param state
 * @param message$
 */
export const messageStreamEpic = (action$, state, message$ = messageEvent$) => action$.pipe(
    ofType(APP_INIT),
    switchMap(action => message$.pipe(
        map(([key, message]) => ({
            key,
            message: JSON.parse(message)
        })),
        map(({key, message}) => ({
            type: NEW_MESSAGE,
            payload: {
                channel: key,
                message
            }
        })),
        catchError(error => {
            console.error(error)
        })
    ))
);

/**
 * Listen to packets being send out
 * @param action$
 * @param state
 * @param packetsSend$
 */
export const packetSendEpic = (action$, state, packetsSend$ = packetsSendEvent$) => action$.pipe(
    ofType(APP_INIT),
    switchMap(action => packetsSend$.pipe(
        filter(({cmd}) => cmd === 'publish'),
        map((payload) => ({
            type: PACKET_SEND,
            payload
        })),
        catchError(error => {
            console.error(error)
        })
    ))
);

/**
 * Publish the message on the user channel
 * @param action$
 */
export const sendMessageEpic = action$ => action$.pipe(
    ofType(START_SEND_MESSAGE),
    switchMap(({payload}) => {
        const publish$ = from(client.publish(channelUserMessage, JSON.stringify({
            ...payload,
            username,
            retain: true,
            qos: 1
        })));

        return publish$.pipe(
            map(() => ({
                type: NEW_MESSAGE,
                payload: {
                    channel: channelUserMessage,
                    message: payload
                }
            }))
        );
    })
);

/**
 * Publish the intent on the user intent channel
 * @param action$
 */
export const sendIntentEpic = action$ => action$.pipe(
    ofType(START_SEND_INTENT),
    switchMap(({payload}) => {
        const publish$ = from(client.publish(channelUserIntent, JSON.stringify({
            ...payload,
            username,
            retain: true,
            qos: 1
        })));

        const {app, intent} = payload;

        return publish$.pipe(
            map(() => ({
                type: INTENT_SEND,
                payload: {
                    app,
                    intent
                }
            }))
        );
    })
);