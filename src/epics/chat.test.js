import {ActionsObservable} from 'redux-observable';
import {of} from 'rxjs';
import {toArray} from 'rxjs/operators';

import {
    NEW_MESSAGE,
    PACKET_SEND,
    sendMessage
} from '../actions/chat';

import {appInit} from "../actions";

import {
    sendMessageEpic,
    messageStreamEpic,
    packetSendEpic,
    channelChatBot,
    channelUser
} from "./chat";

describe('Send and Receive Message Epics', () => {
    it('should make SEND_MESSAGE into a NEW_MESSAGE', (done) => {
        const content = 'How is the garden?';
        const channel = channelUser;
        const action$ = ActionsObservable.of(sendMessage(content));

        const output$ = sendMessageEpic(action$);
        output$.pipe(toArray()).subscribe(actions => {
            expect(actions[0].type).toEqual(NEW_MESSAGE);
            expect(actions[0].payload).toEqual({channel, content});
            done();
        });
    });

    it('should receive messages via MQTT and transform it to NEW_MESSAGE action', (done) => {
        const content = 'Your garden is happy!';
        const channel = channelChatBot
        const messageEvent$ = of([channel, content], ['rabot/chat/chatbot','Sunny days..']);
        const action$ = ActionsObservable.of(appInit());
        const output$ = messageStreamEpic(action$, null, messageEvent$);

        output$.pipe(toArray()).subscribe(actions => {
            expect(actions[0].type).toEqual(NEW_MESSAGE);
            expect(actions[0].payload).toEqual({channel, content});
            done();
        });
    });

    it('should verify packages being send out -> PACKET_SEND', (done) => {
        const cmd = 'publish';
        const messageId = 32523;
        const packetSendEvent$ = of({cmd, messageId}, {cmd: 'publish', messageId: 32525});

        const action$ = ActionsObservable.of(appInit());
        const output$ = packetSendEpic(action$, null, packetSendEvent$);

        output$.pipe(toArray()).subscribe(actions => {
            expect(actions[0].type).toEqual(PACKET_SEND);
            expect(actions[0].payload).toEqual({cmd, messageId});
            done();
        });
    });
});