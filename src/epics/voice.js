import {ofType} from 'redux-observable';
import {switchMap, map, tap, mapTo, takeWhile} from 'rxjs/operators';

import {APP_INIT} from '../actions';
import {sendMessage, newMessage} from '../actions/chat';

import {
    recordingVoice,
    initVoice,
    START_VOICE_RECORDING,
    STOP_VOICE_RECORDING,
    VOICE_INIT
} from '../actions/voice';

import {
    SpeechRecognition, 
    SpeechResult$, 
    SpeechEnd$,
    SpeechError$,
    SpeechChannel,
    SpeechEnabledDevice
} from '../helpers/speechRecognition';

/**
 * Check if Speech recognition is available in this browser
 * @param action$
 * @param state
 * @param recognition
 */
export const isVoiceEnabledEpic = (action$, state, recognition = SpeechRecognition) => action$.pipe(
    ofType(APP_INIT),
    takeWhile(SpeechEnabledDevice),
    mapTo(initVoice())
);

/**
 * Start recording a voice
 * @param action$
 * @param state
 * @param recognition
 */
export const startVoiceEpic = (action$, state, recognition = SpeechRecognition) => action$.pipe(
    ofType(START_VOICE_RECORDING),
    tap(() => recognition.start()),
    mapTo(recordingVoice(true))
);

/**
 * Stop recording a voice
 * @param action$
 * @param state
 * @param recognition
 */
export const stopVoiceEpic = (action$, state, recognition = SpeechRecognition) => action$.pipe(
    ofType(STOP_VOICE_RECORDING),
    tap(() => recognition.stop()),
    mapTo(recordingVoice(false))
);

/**
 * Get the voice in text format
 * @param action$
 * @param state
 * @param recognitionResult$
 */
export const voiceToTextEpic = (action$, state, recognitionResult$ = SpeechResult$) => action$.pipe(
    ofType(VOICE_INIT),
    switchMap(() => recognitionResult$.pipe(
        map(({results}) => sendMessage(results[0][0].transcript))
    ))
);

/**
 * React to ending of a voice recording
 * @param action$
 * @param state
 * @param recognitionEnd$
 */
export const voiceRecordingEndEpic = (action$, state, recognitionEnd$ = SpeechEnd$) => action$.pipe(
    ofType(VOICE_INIT),
    switchMap(() => recognitionEnd$.pipe(
        map(() => recordingVoice(false))
    ))
);

/**
 * Respond to errors, ex "no-speech"
 * @param action$
 * @param state
 * @param recognitionError$
 */
export const voiceErrorEpic = (action$, state, recognitionError$ = SpeechError$) => action$.pipe(
    ofType(VOICE_INIT),
    switchMap(() => {
        return recognitionError$.pipe(
            map(({error}) => {
                switch (error) {
                    case "no-speech":
                        return newMessage("Sorry, wa?", SpeechChannel);
                    default:
                        return newMessage(`Speech recognition error: ${error}`, SpeechChannel);
                }
            })
        );
    })
);