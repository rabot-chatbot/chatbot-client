import {ofType} from 'redux-observable';
import {fromEvent, merge} from 'rxjs';
import {switchMap, map, catchError, startWith} from 'rxjs/operators';

import {
    APP_INIT
} from '../actions';

const ONLINE = 'online';
const OFFLINE = 'offline';

/**
 * Listen to events from Online and Offline
 * @type {Observable<T>}
 */
const onlineEvent$ = fromEvent(window, ONLINE);
const offlineEvent$ = fromEvent(window, OFFLINE);

const onlineOrOfflineEvents$ = merge(onlineEvent$, offlineEvent$).pipe(
    startWith({
        type: navigator.onLine ? ONLINE : OFFLINE
    })
);

/**
 * Listen to incoming messages
 * @param action$
 * @param state
 * @param onlineOrOffline$
 */
export const networkChangeEpic = (action$, state, onlineOrOffline$ = onlineOrOfflineEvents$) => action$.pipe(
    ofType(APP_INIT),
    switchMap(action => onlineOrOffline$.pipe(
        map(() => ({
            type: 'ONLINE',
            payload: navigator.onLine
        })),
        catchError(error => {
            console.error(error)
        })
    ))
);
