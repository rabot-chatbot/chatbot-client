import {combineEpics} from 'redux-observable';
import {messageStreamEpic, sendMessageEpic, packetSendEpic, sendIntentEpic} from './chat';
import {networkChangeEpic} from "./network";

import {
    startVoiceEpic,
    voiceToTextEpic,
    stopVoiceEpic,
    voiceRecordingEndEpic,
    voiceErrorEpic,
    isVoiceEnabledEpic
} from './voice';

export const rootEpic = combineEpics(
    messageStreamEpic,
    sendMessageEpic,
    sendIntentEpic,
    packetSendEpic,
    networkChangeEpic,
    startVoiceEpic,
    stopVoiceEpic,
    voiceErrorEpic,
    voiceRecordingEndEpic,
    voiceToTextEpic,
    isVoiceEnabledEpic
);