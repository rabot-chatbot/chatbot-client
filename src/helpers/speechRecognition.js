import { fromEvent } from 'rxjs';

/**
 * Init SpeechRecognition for VTT
 * @returns {null}
 */
function initRecognition () {
    const SpeechRecognition = SpeechRecognition || window.webkitSpeechRecognition;

    if (SpeechRecognition) {
        return new SpeechRecognition();
    }

    return null;
}

/**
 * Create an Speech event observable
 * @param recognition
 * @param event
 * @returns {*}
 */
function speechEventObs (recognition, event) {
    if (recognition) {
        return fromEvent(recognition, event);
    }

    return null;
}

export const SpeechRecognition = initRecognition();
export const SpeechEnabledDevice = () => Boolean(SpeechRecognition);
export const SpeechChannel = 'speech/recognition';

export const SpeechResult$ = speechEventObs(SpeechRecognition, 'result');
export const SpeechEnd$ = speechEventObs(SpeechRecognition, 'audioend');
export const SpeechError$ = speechEventObs(SpeechRecognition, 'error');