import React from 'react';
import { connect } from 'react-redux';

import './Toolbar.css';

import MessageIcon from '../Icons/MessageIcon'

const Toolbar = ({ online }) => {
    return (
        <div className="Toolbar">
            <div className="Toolbar__MenuIcon">
                <MessageIcon />
            </div>
        </div>
    );
};

export default connect(
    ({network: {online}}) => ({online})
)(Toolbar);