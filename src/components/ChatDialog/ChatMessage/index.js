import React, {PureComponent} from 'react';
import classNames from 'classnames';

import {channelUserMessage} from '../../../epics/chat';

import ChatSuggestions from '../ChatSuggestions';

import './ChatMessage.css';

class ChatMessage extends PureComponent {
    render () {
        const {message: {utterance, replyTo, matchList, type}, channel} = this.props;

        const messageClassNames = classNames('ChatMessage__container', {
            'ChatMessage__container--user': channel === channelUserMessage
        });

        return (
            <div className={messageClassNames}>
                <div className="ChatMessage__text">
                    {utterance}
                </div>
                {type === "mismatch" && <ChatSuggestions replyTo={replyTo} suggestions={matchList} />}
            </div>
        )
    }
}

export default ChatMessage;