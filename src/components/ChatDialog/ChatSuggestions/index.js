import React, {PureComponent} from 'react';
import {connect} from "react-redux";

import { sendIntent } from '../../../actions/chat';

import './ChatSuggestions.css';

class ChatSuggestions extends PureComponent {
    render() {
        const {replyTo, suggestions, sendIntent} = this.props;

        return (
            <div className="ChatSuggestions">
                {suggestions.map(({label, app, intent}, index) => <button key={index} onClick={() => sendIntent(app, intent, replyTo)}>{label}</button>)}
            </div>
        )
    }
}

export default connect(
    null,
    {sendIntent}
)(ChatSuggestions);