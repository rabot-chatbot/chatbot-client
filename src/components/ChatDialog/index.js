import React from 'react';
import {connect} from 'react-redux';
import ReactChatView from 'react-chatview';

import './ChatDialog.css';

import ChatMessage from './ChatMessage';

class ChatDialog extends React.Component {
    render() {
        const {messages} = this.props;

        return (
            <div className="ChatDialog">
                <ReactChatView
                    className="message-list ChatDialog__list"
                    flipped={true}
                    scrollLoadThreshold={50}
                    onInfiniteLoad={this.loadMoreHistory.bind(this)}
                >
                    {messages.map((content, index) => <ChatMessage key={index} {...content} />)}
                </ReactChatView>
            </div>
        );
    }

    /**
     * Load more items from the archive
     */
    loadMoreHistory () {
        console.log('Implement load more from archive');
        // TODO: Implement load more from archive
    }
}

export default connect(
    ({chat: {messages}}) => ({messages})
)(ChatDialog);