import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';

import './NetworkIndicator.css';

const NetworkIndicator = ({ online }) => {
    const networkClassNames = classNames('NetworkIndicator', {
        'NetworkIndicator--offline': !online
    });

    return (
        <div className={networkClassNames}></div>
    );
};

export default connect(
    ({network: {online}}) => ({online})
)(NetworkIndicator);