import React from 'react';
import {connect} from 'react-redux';

import {appInit} from '../actions';

import ChatForm from './ChatForm';
import ChatDialog from './ChatDialog';
import Toolbar from './Toolbar';
import NetworkIndicator from './NetworkIndicator';

import './App.css';

class App extends React.Component {
    componentDidMount() {
        this.props.appInit();
    }

    render() {
        return (
            <div className="App">
                <div className="App__Header">
                    <NetworkIndicator />
                    <Toolbar />
                </div>

                <div className="App__Center">
                    <ChatDialog />
                </div>

                <div className="App__Footer">
                    <ChatForm />
                </div>
            </div>
        );
    }
}

export default connect(
    null,
    {appInit}
)(App);