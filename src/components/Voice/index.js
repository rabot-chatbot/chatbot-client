import React from 'react';
import { connect } from 'react-redux';
import classNames from 'classnames';

import './Voice.css';

import MicIcon from '../Icons/MicIcon'

import { recordVoice, stopRecordVoice } from '../../actions/voice';

const Voice = ({ recordVoice, stopRecordVoice, enabled, recording }) => {
    if (!enabled) return null;

    const voiceClassNames = classNames('Voice', {
       'Voice--Recording': recording
    });

    return (
        <div className={voiceClassNames} onClick={() => {
            if (recording) {
                stopRecordVoice();
            } else {
                recordVoice();
            }
        }}>
            <MicIcon />
        </div>
    )
};

export default connect(
    ({voice: {enabled, recording}}) => ({enabled, recording}),
    { recordVoice, stopRecordVoice }
)(Voice);