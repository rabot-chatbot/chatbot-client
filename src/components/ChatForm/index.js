import React from 'react';
import { connect } from 'react-redux';

import Voice from '../Voice';

import { sendMessage } from '../../actions/chat';

import './ChatForm.css';

class ChatForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {value: ''};

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(event) {
        this.setState({value: event.target.value});
    }

    handleSubmit(event) {
        event.preventDefault();
        this.props.sendMessage(this.state.value);
        this.setState({value: ''});
    }

    render() {
        return (
            <div className="ChatForm">
                <div className="ChatForm__write">
                    <form className="ChatForm__form" onSubmit={this.handleSubmit}>
                        <input type="text" className="ChatForm__input" value={this.state.value} autoFocus onChange={this.handleChange} />
                    </form>
                </div>
                <div className="ChatForm__buttons">
                    <Voice />
                </div>
            </div>
        );
    }
}

export default connect(
    () => ({}),
    { sendMessage }
)(ChatForm)