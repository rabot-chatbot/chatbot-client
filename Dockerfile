# You should always specify a full version here to ensure all of your developers
# are running the same version of Node.
FROM node:8.0

# Override the base log level (info).
ENV NPM_CONFIG_LOGLEVEL warn

# Install and configure `serve`.
RUN npm install -g serve
RUN npm install -g react-scripts
CMD serve -s build
EXPOSE 5000

# Install all dependencies of the current project.
COPY package*.json ./
RUN npm install --production

# Copy all local files into the image.
COPY . .

# Build for production.
RUN npm run build --production

# Remove react-scripts only needed for building
RUN npm uninstall -g react-scripts