const Voice = '.Voice';

describe('Voice', () => {
    beforeEach(() => {
        cy.visit('/');
    });

    it('turns the icon red and disabled after clicking', () => {
        cy.get(Voice).as('voiceButton');

        cy.get('@voiceButton')
            .click()
            .should('have.class', 'Voice--Recording');
    });
});