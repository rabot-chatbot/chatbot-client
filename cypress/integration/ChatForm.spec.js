const ChatForm__input = '.ChatForm__input';
const ChatDialog__list = '.ChatDialog__list';
const ChatSuggestions = '.ChatSuggestions';

describe('ChatForm', () => {
    beforeEach(() => {
        cy.visit('/');
    });

    it('input is focused on load', () => {
        cy.focused()
            .should('have.class', 'ChatForm__input');
    });

    it('accepts input', () => {
        const typedText = 'How is the weather?';

        cy.get(ChatForm__input)
            .type(typedText)
            .should('have.value', typedText)
    });

    context('Form submission', () => {
       it('Adds the new text to the conversation on submit and a response from Moes', () => {
           const items = [
               {text: 'Does the garden need water?', contains: 'Moist sensor'},
               // {text: 'How is the weather?', contains: 'Sunny'},
           ];

           cy.wrap(items)
               .each(item => {
                   cy.get(ChatForm__input)
                       .type(item.text)
                       .type('{enter}')
                       .should('have.value', '')
                       .get(`${ChatDialog__list}`)
                       .and('contain', item.text)
                       .and('contain', item.contains)
               });
       });

       it('When message is not understandable, show a list of possible intents', () => {
           cy.get(ChatForm__input)
               .type('Lorem ipsum')
               .type('{enter}')
               .should('have.value', '')
               .get(`${ChatSuggestions}`)
       });
    });
});